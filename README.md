# @ngcore/data
> NG Core angular/typescript data library


Data/model classes library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/data/



