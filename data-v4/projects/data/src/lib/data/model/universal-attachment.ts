import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { UniqueIdUtil } from '@ngcore/core';
import { BaseModel, ExtendedModel } from '@ngcore/base';


/**
 * "Universal attachment" can be associated with any type of object as long as it makes sense...
 */
export class UniversalAttachment extends ExtendedModel // extends BaseModel 
{
  // Title? label? description?
  public title: (string | null) = null;
  // get title(): string {
  //   return this._title;
  // }
  setTitle(_title: string) {
    this.title = _title;
    this.isDirty = true;
  }


  // Mime type?
  // Supports both binary types (e.g., image, audio) and text types (e.g., commonmark) ????
  public mediaType: (string | null) = null;
  // getMediaType(): string { return this.mediaType; }
  setMediaType(_mediaType: string) { this.mediaType = _mediaType; this.isDirty = true; }

  // External blob storage.
  public mediaUrl: (string | null) = null;
  // getMediaUrl(): string { return this.mediaUrl; }
  setMediaUrl(_mediaUrl: string) { this.mediaUrl = _mediaUrl; this.isDirty = true; }


  // isDeleted?
  // isHidden?
  public isHidden: boolean = false;
  // getIsHidden(): boolean { return this.isHidden; }
  setIsHidden(_isHidden: boolean) { this.isHidden = _isHidden; this.isDirty = true; }

  // tags/labels
  // labels: string[] = [];  // ???
  public isFavorite: boolean = false;
  // getIsFavorite(): boolean { return this.isFavorite; }
  setIsFavorite(_isFavorite: boolean) { this.isFavorite = _isFavorite; this.isDirty = true; }
  // ...


  // public taskId: string = null
  // // getTaskId(): string { return this._taskId; }
  // setTaskId(_taskId: string) { this.taskId = _taskId; this.isDirty = true; }

  constructor(public hostObjectType: (string | null) = null, public hostObjectMember: ([string, any] | null) = null) {
    super();
  }


  toString(): string {
    return super.toString()
      + '; hostObjectType = ' + this.hostObjectType
      + '; hostObjectMember = ' + this.hostObjectMember
      + '; title = ' + this.title
      + '; mediaType = ' + this.mediaType
      + '; mediaUrl = ' + this.mediaUrl
      + '; isHidden = ' + this.isHidden
      + '; isFavorite = ' + this.isFavorite;
  }


  clone(): UniversalAttachment {
    let cloned = Object.assign(new UniversalAttachment(), this) as UniversalAttachment;
    return cloned;
  }
  static clone(obj: any): UniversalAttachment {
    let cloned = Object.assign(new UniversalAttachment(), obj) as UniversalAttachment;
    return cloned;
  }

  copy(): UniversalAttachment {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    obj.resetCreatedTime();
    return obj;
  }
}
