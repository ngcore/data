import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { UniqueIdUtil } from '@ngcore/core';
import { BaseModel } from '@ngcore/base';


/**
 * A "universal note" can be associated with any object.
 */
// TBD: Allow more than one notes per object (or, per user) ???
export class UniversalNote extends BaseModel {

  // Title?
  public title: (string | null) = null;
  // get title(): string {
  //   return this._title;
  // }
  setTitle(_title: string) {
    this.title = _title;
    this.isDirty = true;
  }


  // Plain text vs Markdown vs HTML ???
  // --> Just use commonmark...
  // public mediaType: string = null;
  // // getMediaType(): string { return this.mediaType; }
  // setMediaType(_mediaType: string) { this.mediaType = _mediaType; this.isDirty = true; }


  // TBD: Just store markdownText only???

  // Not being used...

  // Raw input text, in markdown.
  public rawText: (string | null) = null;
  // // Sanitized, to avoid bad input...
  // public markdownText: string;
  // Text without markups.
  public plainText: (string | null) = null;
  // Output in HTML
  public htmlText: (string | null) = null;


  // Currently, only markdownText is used.
  public markdownText: (string | null) = null;
  // get markdownText(): string {
  //   return this._markdownText;
  // }
  setMarkdownText(_markdownText: string) {
    this.markdownText = _markdownText;
    this.isDirty = true;
  }


  // isDeleted?
  // isHidden?
  public isHidden: boolean = false;
  // getIsHidden(): boolean { return this.isHidden; }
  setIsHidden(_isHidden: boolean) { this.isHidden = _isHidden; this.isDirty = true; }


  // public taskId: string = null
  // // getTaskId(): string { return this._taskId; }
  // setTaskId(_taskId: string) { this.taskId = _taskId; this.isDirty = true; }

  // // Note target date in dateId format. (Based on createdTime? No.)
  // // To be used only when taskId == null.
  // public noteDate: string;

  // // Object type and the target field/attr key-value pair (e.g., id, targetDate,...).
  // // (These should be considered immutable.)
  // public hostObjectType: string;
  // public hostObjectMember: [string, any];

  constructor(public hostObjectType: (string | null) = null, public hostObjectMember: ([string, any] | null) = null) {
    super();
  }


  toString(): string {
    return super.toString()
      + '; hostObjectType = ' + this.hostObjectType
      + '; hostObjectMember = ' + this.hostObjectMember
      + '; title = ' + this.title
      + '; markdownText = ' + this.markdownText
      + '; isHidden = ' + this.isHidden;
  }


  clone(): UniversalNote {
    let cloned = Object.assign(new UniversalNote(), this) as UniversalNote;
    if (this.hostObjectMember && this.hostObjectMember.length == 2) cloned.hostObjectMember = this.hostObjectMember.slice(0) as [string, any];
    return cloned;
  }
  static clone(obj: any): UniversalNote {
    let cloned = Object.assign(new UniversalNote(), obj) as UniversalNote;
    if (obj.hostObjectMember && obj.hostObjectMember.length == 2) cloned.hostObjectMember = obj.hostObjectMember.slice(0) as [string, any];
    return cloned;
  }

  copy(): UniversalNote {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    obj.resetCreatedTime();
    return obj;
  }
}
