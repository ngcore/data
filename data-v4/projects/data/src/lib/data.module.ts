import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';

export * from './common/events/data-events';
export * from './common/util/common-data-util';
export * from './data/model/universal-note';
export * from './data/model/universal-attachment';
export * from './data/model/user-profile';
export * from './data/model/user-preference';


@NgModule({
  imports: [
    CommonModule,
    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
  ],
  declarations: [
  ],
  exports: [
  ],
  entryComponents: [
    // ????
  ]
})
export class NgCoreDataModule {
  static forRoot(): ModuleWithProviders<NgCoreDataModule> {
    return {
      ngModule: NgCoreDataModule,
      providers: [
        // CommonDataService
      ]
    };
  }
}
